package com.sda.rest.dao.model;

public enum LOGIN_RESULT {
	SUCCESS,	// ok, logged in
	GENERAL_FAILURE, // server error
	WRONG_PASSWORD, 
	REGISTERED // than register this account
}
