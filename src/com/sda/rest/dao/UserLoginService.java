package com.sda.rest.dao;

import java.util.LinkedList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import com.sda.rest.HibernateSession;
import com.sda.rest.dao.model.LOGIN_RESULT;
import com.sda.rest.dao.model.LoginResult;
import com.sda.rest.model.Location;
import com.sda.rest.model.Message;
import com.sda.rest.model.Token;
import com.sda.rest.model.User;

public class UserLoginService {

	public UserLoginService() {

	}

	public LoginResult tryLogin(String login, String password) {
		Session session = HibernateSession.getSessionFactory().openSession();
		Transaction t = session.beginTransaction();
		try {
			Criteria criteria = session.createCriteria(User.class);
			User result = (User) criteria.add(Restrictions.eq("login", login))
					.uniqueResult();

			if (result != null) {
				if (result.getPassword_hash().equals(password)) {
					Token userToken = TokenService.getToken(result.getId());
					
					userToken.setToken(generateToken());
					
					session.update(userToken);
					
					return LoginResult.successfulResult(result, userToken);
				} else {
					return LoginResult.failedPassword();
				}
			}
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			return LoginResult.failedError();
		} finally {
			t.commit();
			if (session != null) {
				session.close();
			}
		}

		return register(login, password);
	}

	private String generateToken() {
		String md5 = String.valueOf(System.currentTimeMillis());
		try {
			java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
			byte[] array = md.digest(md5.getBytes());
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < array.length; ++i) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
			}
			return sb.toString();
		} catch (java.security.NoSuchAlgorithmException e) {
		}
		return null;
	}

	private LoginResult register(String login, String password) {
		Session session = HibernateSession.getSessionFactory().openSession();
		User user = new User(login, password);
		Token token = null;
		try {
			session.beginTransaction();
			session.save(user);

			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
		session = HibernateSession.getSessionFactory().openSession();

		try {
			session.beginTransaction();
			token = new Token(user.getId(), generateToken());
			session.save(token);

			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

		session = HibernateSession.getSessionFactory().openSession();

		try {
			session.beginTransaction();
			Location loc  = new Location(user.getId(), 0.0, 0.0, "");
			session.save(loc);

			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
		
		return LoginResult.registered(user, token);
	}
}
