package com.sda.rest.dao;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.sda.rest.HibernateSession;
import com.sda.rest.model.Message;
import com.sda.rest.model.User;

public class ConversationService {

	public ConversationService() {
		// TODO Auto-generated constructor stub
	}

	public List<Message> getConversationBetweenUsers(int sender_id, int recipient_id) {
		List<Message> returnList = new LinkedList<>();

		Session session = HibernateSession.getSessionFactory().openSession();
		Transaction t = session.beginTransaction();
		try {
			Criteria criteria = session.createCriteria(Message.class);

			List messages = criteria.add(Restrictions.in("sender_id", new Object[] { sender_id, recipient_id }))
					.add(Restrictions.in("recipient_id", new Object[] { sender_id, recipient_id }))
					.add(Restrictions.in("status", new Object[] { 3, 1})).addOrder(Order.asc("timestamp")).list();

			
			for (Iterator iterator = messages.iterator(); iterator.hasNext();) {
				Message m = (Message) iterator.next();
				int currentStatus = m.getStatus();
				if ((m.getRecipient_id() == sender_id)) {
					if ((currentStatus == 1 || currentStatus ==0)) {
						currentStatus += 2;
					}
				}
			
				m.setStatus(currentStatus);
				session.update(m);
			}

			returnList.addAll(messages);
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
		} finally {
			t.commit();
			if (session != null) {
				session.close();
			}
		}

		return returnList;
	}

	public boolean sendMessage(int sender_id, int recipient_id, String content) {
		Session session = HibernateSession.getSessionFactory().openSession();
		Transaction t = session.beginTransaction();
		try {

			Message m = new Message(content, sender_id, recipient_id);
			session.save(m);

			return true;
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
		} finally {
			t.commit();
			if (session != null) {
				session.close();
			}
		}

		return false;
	}

	public List<Message> getUnreadMessages(int sender_id, int recipient_id) {
		List<Message> returnList = new LinkedList<>();

		Session session = HibernateSession.getSessionFactory().openSession();
		Transaction t = session.beginTransaction();
		try {
			Criteria criteria = session.createCriteria(Message.class);

			List<Message> messages = criteria.add(Restrictions.in("sender_id", new Object[] { sender_id, recipient_id }))
					.add(Restrictions.in("recipient_id", new Object[] { sender_id, recipient_id }))
					.add(Restrictions.ne("status", 3)).addOrder(Order.asc("timestamp")).list();
			returnList.addAll(messages);

			for (Iterator iterator = messages.iterator(); iterator.hasNext();) {
				Message m = (Message) iterator.next();
				int currentStatus = m.getStatus();
				if ((m.getRecipient_id() == sender_id)) {
					if ((currentStatus == 1 || currentStatus ==0)) {
						currentStatus += 2;
					} else {
						returnList.remove(m);
						continue;
					}
				}
				if ((m.getSender_id() == sender_id)) {
					if ((currentStatus == 2 || currentStatus == 0)) {
						currentStatus += 1;
					}else{
						returnList.remove(m);
						continue;
					}
				}

				m.setStatus(currentStatus);
				session.update(m);
				
			}
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			e.printStackTrace();
		} finally {
			t.commit();
			if (session != null) {
				session.close();
			}
		}

		return returnList;
	}
}
