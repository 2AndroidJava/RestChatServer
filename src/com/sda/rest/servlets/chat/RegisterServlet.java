package com.sda.rest.servlets.chat;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

import com.sda.rest.dao.TokenService;
import com.sda.rest.dao.UserLoginService;
import com.sda.rest.dao.model.LOGIN_RESULT;
import com.sda.rest.dao.model.LoginResult;

@Path("/register")
public class RegisterServlet {
	/**
	 * Rejestruje u�ytkownika (je�li nie istnieje) lub pr�buje go zalogowa�
	 * (je�li istnieje).
	 * 
	 * @param data
	 * @return
	 * @throws JSONException
	 */
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	public Response register(String data) throws JSONException {
		UserLoginService service = new UserLoginService();
		JSONObject responseResult = new JSONObject();

		JSONObject obj = new JSONObject(data);
		LoginResult result = service.tryLogin(obj.getString("login"), obj.getString("password_hash"));

		return Response.status(200).entity(result.getJSONResult().toString()).build();
	}
}