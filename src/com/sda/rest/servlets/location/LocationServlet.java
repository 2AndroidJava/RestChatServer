package com.sda.rest.servlets.location;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

import com.sda.rest.dao.location.UserLocationService;
import com.sda.rest.model.Location;

@Path("/")
public class LocationServlet {

	@POST
	@Path("/getLocation")
	@Produces("application/json")
	@Consumes("application/json")
	public Response getLocation(String data) throws JSONException {
		UserLocationService service = new UserLocationService();
		JSONObject obj = new JSONObject(data);
		int user_id = obj.getInt("user_id");

		Location location= service.getLocation(user_id);

		JSONObject responseResult = new JSONObject();
		responseResult.put("result", new JSONObject(location));
		return Response.status(200).entity(responseResult.toString()).build();
	}

	@POST
	@Path("/setLocation")
	@Produces("application/json")
	@Consumes("application/json")
	public Response setLocation(String data) throws JSONException {
		UserLocationService service = new UserLocationService();
		JSONObject obj = new JSONObject(data);
		
		int user_id = obj.getInt("user_id");
		double lat = obj.getDouble("lat");
		double lon = obj.getDouble("lon");
		String comment = obj.getString("comment");
		
		service.setLocation(new Location(user_id, lat, lon, comment));
		
		JSONObject responseResult = new JSONObject();
		return Response.status(200).entity(responseResult.toString()).build();
	}
	
//	@POST
//	@Path("/clearLocation")
//	@Produces("application/json")
//	@Consumes("application/json")
//	public Response clearLocation(String data) throws JSONException {
//		UserLocationService service = new UserLoginService();
//		JSONObject responseResult = new JSONObject();
//
//		JSONObject obj = new JSONObject(data);
//		LoginResult result = service.tryLogin(obj.getString("login"), obj.getString("password_hash"));
//
//		return Response.status(200).entity(result.getJSONResult().toString()).build();
//	}
}