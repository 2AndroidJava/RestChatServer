package com.sda.rest.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="user_token", uniqueConstraints = { @UniqueConstraint(columnNames = "user_id") })
public class Token {

	@Id
	@Column(name="user_id")
	private int user_id;
	
	@Column(name="token")
	private String token;
	
	@Column(name="token_timestamp")
	private String token_timestamp;

	public Token() {
		// TODO Auto-generated constructor stub
	}
	
	public Token(int user_id, String token){
		this.user_id = user_id;
		this.token = token;
		this.token_timestamp = String.valueOf(System.currentTimeMillis());
	}
	
	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		updateTimestamp();
		this.token = token;
	}

	public String getToken_timestamp() {
		return token_timestamp;
	}

	public void updateTimestamp() {
		this.token_timestamp = String.valueOf(System.currentTimeMillis());
	}
	
	public void setToken_timestamp(String token_timestamp) {
		this.token_timestamp = token_timestamp;
	}
}
