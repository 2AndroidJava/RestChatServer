package com.sda.rest.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.sda.rest.HibernateSession;
import com.sda.rest.config.Config;
import com.sda.rest.dao.model.LOGIN_RESULT;
import com.sda.rest.model.Token;
import com.sda.rest.model.User;

public class TokenService {

	public TokenService() {
		// TODO Auto-generated constructor stub
	}
	
	public static boolean verifyToken(int user_id, String token){
		Session session = HibernateSession.getSessionFactory().openSession();
		Transaction tr = session.beginTransaction();
		try {
			Criteria criteria = session.createCriteria(Token.class);
			Token result = (Token) criteria.add(Restrictions.eq("user_id", user_id))
					.add(Restrictions.eq("token", token)).uniqueResult();

			if( result != null){
				Long timestampToken = Long.parseLong(result.getToken_timestamp());
				Long difference = System.currentTimeMillis() - timestampToken;
				
				if( difference < Config.TIMESTAMP_TIMEOUT){
					result.updateTimestamp();
					session.update(result);
					return true;
				}
			}
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
		} finally {
			tr.commit();
			if (session != null) {
				session.close();
			}
		}

		return false;
	}

	public static Token getToken(int user_id){
		Token t = null;
		Session session = HibernateSession.getSessionFactory().openSession();
		Transaction tr = session.beginTransaction();
		try {
			Criteria criteria = session.createCriteria(Token.class);
			Token result = (Token) criteria.add(Restrictions.eq("user_id", user_id)).uniqueResult();

			t = result;
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
		} finally {
			tr.commit();
			if (session != null) {
				session.close();
			}
		}

		return t;
	}
	
	public static void updateToken(int user_id, String token){
		Token t = getToken(user_id);
		t.setToken(token);
		t.updateTimestamp();
		
		Session session = HibernateSession.getSessionFactory().openSession();
		Transaction tr = session.beginTransaction();
		try {
			session.beginTransaction();
			session.update(t);

			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
}
