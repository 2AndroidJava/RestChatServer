package com.sda.rest.dao.model;

import org.json.JSONObject;

import com.sda.rest.model.Token;
import com.sda.rest.model.User;

public class LoginResult {
	private LOGIN_RESULT result;
	private User user;
	private Token token;

	private LoginResult(LOGIN_RESULT result, User user, Token token) {
		super();
		this.result = result;
		this.user = user;
		this.token = token;
	}

	public JSONObject getJSONResult() {
		JSONObject resObj = new JSONObject();
		try {
			resObj.put("result", result);
			resObj.put("id", user.getId());
			resObj.put("token", token.getToken());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return resObj;
	}

	public static LoginResult successfulResult(User u, Token t) {
		return new LoginResult(LOGIN_RESULT.SUCCESS, u, t);
	}

	public static LoginResult failedPassword() {
		return new LoginResult(LOGIN_RESULT.WRONG_PASSWORD, null, null);
	}

	public static LoginResult failedError() {
		return new LoginResult(LOGIN_RESULT.GENERAL_FAILURE, null, null);
	}

	public static LoginResult registered(User u, Token t) {
		return new LoginResult(LOGIN_RESULT.REGISTERED, u, t);
	}
}
