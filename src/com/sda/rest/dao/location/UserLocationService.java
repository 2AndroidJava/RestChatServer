package com.sda.rest.dao.location;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.sda.rest.HibernateSession;
import com.sda.rest.dao.TokenService;
import com.sda.rest.dao.model.LoginResult;
import com.sda.rest.model.Location;
import com.sda.rest.model.Token;
import com.sda.rest.model.User;

public class UserLocationService {
	public UserLocationService() {
		// TODO Auto-generated constructor stub
	}

	public void setLocation(Location location){
		Session session = HibernateSession.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			session.update(location);

			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public Location getLocation(int user_id) {
		Session session = HibernateSession.getSessionFactory().openSession();
		Transaction t = session.beginTransaction();
		try {
			Criteria criteria = session.createCriteria(Location.class);
			Location result = (Location) criteria.add(Restrictions.eq("user_id", user_id))
					.uniqueResult();

			return result;
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
		} finally {
			t.commit();
			if (session != null) {
				session.close();
			}
		}

		return null;
	}
}
