package com.sda.rest.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="user_location", uniqueConstraints = { @UniqueConstraint(columnNames = "user_id") })
public class Location {

	@Id
	@Column(name ="user_id")
	private int user_id;
	
	@Column(name="lat")
	private double lat;

	@Column(name="lon")
	private double lon;
	
	@Column(name="comment")
	private String comment;
	
	public Location(int user_id, double lat, double lon, String comment) {
		super();
		this.user_id = user_id;
		this.lat = lat;
		this.lon = lon;
		this.comment = comment;
	}
	
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

	public Location() {
		// TODO Auto-generated constructor stub
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}
}
